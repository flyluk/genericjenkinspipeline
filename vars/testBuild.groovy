import groovy.json.JsonOutput
import groovy.json.JsonBuilder
def call (Map configuration)
{
    log.setDefaultNodeVer(configuration)
    
    pipeline {
         environment { 
              CC = sh(script:"echo $PATH",returnStdout: true).trim()
        }
        
        agent { 
            docker { 
                image "flyluk/busybox:1.0"
                args "-u root -e PATH=${configuration.path}"
            }
        }
       

        stages{
            stage("Test")
            {
                steps{
                    script {
                        checkout([$class: 'GitSCM', 
                                  branches: [[name: configuration.branch]], 
                                  doGenerateSubmoduleConfigurations: false, 
                                  extensions: [], 
                                  submoduleCfg: [], 
                                  userRemoteConfigs: [[credentialsId: 'fly.luk@gmail.com', 
                                                       url: 'https://flyluk@bitbucket.org/flyluk/mypipeline.git']]])
                        log.info 'Starting'
                        log.info this.class.name
                        log.warning 'Nothing to do!'
                        def builder = new JsonBuilder()
                        builder configuration
                        println JsonOutput.prettyPrint(builder.toString())
                    }
                 }
            }
            stage("Print")
            {
                steps {
                    script {
                       
                         sh "env"
                         sh "node -v"
                     }
                }
            }
        }

    /* insert Declarative Pipeline here */
    }
}