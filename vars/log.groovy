def info(message) {
    echo "INFO: ${message}"
}

def warning(message) {
    echo "WARNING: ${message}"
}

def setDefaultNodeVer(configuration)
{
    if ( configuration.nodeVersion == null )
        configuration.nodeVersion = "11.9.0"
    
    configuration.path="/root/.nvm/versions/node/v"+configuration.nodeVersion+"/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
}
